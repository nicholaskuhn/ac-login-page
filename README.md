* To use this application, you must first sign in. To sign in, simply click on your username and the application will do the rest.
Only one player may be signed in at a time, and you may not upload your file while you are signed in.
* To search through the museum's database, simply type into the text box in the museum tab. The text will try to match your input
to items in the database. When you wish to add an item, simply type in the item name, select the type (default fossil) and hit 
enter or click on "Add".
* If you would like to look at upcoming events, or add an event yourself, please click on the calendar tab below the museum tab. 
Clicking inside of the text box will pop up a calendar on which you will easily be able to see which dates already have one or more
events on them, indicated by the background of the date. Once you have selected a date, if said date has an event on it, the event
name and description will be shown below, as well as a picture of any special NPC mentioned in the description!
    * To add an event to a date, simply select a date via the calendar as previously mentioned and click the "Add Event" button. The 
application will as you for an event name and description, and once you're finished with that, all it takes is clicking the, "Add"
button or hitting enter and your event will be added! (Don't forget to mention any special NPCs that may be involved in the 
description!)
* In the final tab of the application labeled, "File", you may upload and download the latest save file of your Animal Crossing game!
    * To upload, all you have to do is select, "Choose File" and find the file named, "01-GAFE-DobutsunomoriP_MURA.gci". Make sure you're
    uploading it as a gci file, though! Other file types will not be accepted. Remember, you must be signed out to upload!
    * To download, you need only click on the download file button! If you are prompted whether to open or save the file, please choose to
    save the file into your, "Card A" folder and the game will detect the save file on its own. 
    
* That's it! Good luck and enjoy yourself!

"Database used at: Heliohost via phpMyAdmin"
